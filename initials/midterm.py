#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## BEGIN_SUB_TUTORIAL imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
##

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi, tau, dist, fabs, cos, sqrt, sin, cos

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        ## BEGIN_SUB_TUTORIAL setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group.
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## END_SUB_TUTORIAL

        ## BEGIN_SUB_TUTORIAL basic_info
        ##
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names
    
# ====================BEGINNING OF MY CODE===================================
    def home(self):
        # set the current move group
        move_group = self.move_group
        
        # start pose is a pose I captured earlier as a good drawing start
        start_pose = geometry_msgs.msg.Pose()
        start_pose.position.x = 0.6
        start_pose.position.y = 0.2
        start_pose.position.z = 0.2
        start_pose.orientation.y = sqrt(2)/-2
        start_pose.orientation.z = sqrt(2)/-2

        # set robot to target the start pose
        move_group.set_pose_target(start_pose)
        # go to the target pose
        success = move_group.go(wait=True)
        # from tutorial
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        move_group.clear_pose_targets()
        
    def add_semicircle(self, center, radius, steps, direction, waypoints):
        # add seps number of poses along a semicircle to waypoints list
        for i in range(1, steps):
            # compute theta for each step,
            # then add pose for that theta to the waypoints
            theta = pi * i/(steps-1)
            pose = copy.deepcopy(center)
            pose.position.x += radius * direction * sin(theta)
            pose.position.z -= radius * cos(theta)
            waypoints.append(pose)
    
    def next_letter(self, size):
        # set the current move group
        move_group = self.move_group

        start_pose = move_group.get_current_pose().pose
        start_pose.position.x -= 0.12 * size

        # set robot to target the start pose
        move_group.set_pose_target(start_pose)
        # go to the target pose
        success = move_group.go(wait=True)
        # from tutorial
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        move_group.clear_pose_targets()
    
    def draw_d(self):
        # set the current move group
        move_group = self.move_group

        # create list of waypoints to move through
        waypoints = []

        # save the start pose
        start_pose = move_group.get_current_pose().pose
        print(start_pose)
        # find pose of center of the semicircle to draw
        circle_center = copy.deepcopy(start_pose)
        circle_center.position.z += 0.1

        self.add_semicircle(circle_center, 0.1, 100, -1, waypoints)

        waypoints.append(start_pose)

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        move_group.execute(plan, wait=True)

    def draw_p(self):
        # set the current move group
        move_group = self.move_group

        # create list of waypoints to move through
        waypoints = []

        # save the start pose
        start_pose = move_group.get_current_pose().pose
        print(start_pose)
        # find pose of center of the semicircle to draw
        p_midpoint = copy.deepcopy(start_pose)
        p_midpoint.position.z += 0.1
        waypoints.append(p_midpoint)
        
        circle_center = copy.deepcopy(p_midpoint)
        circle_center.position.z += 0.05

        self.add_semicircle(circle_center, 0.05, 100, -1, waypoints)

        waypoints.append(start_pose)

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        move_group.execute(plan, wait=True)
    
    def draw_c(self):
        # set the current move group
        move_group = self.move_group

        # create list of waypoints to move through
        waypoints = []

        # save the start pose
        start_pose = move_group.get_current_pose().pose
        print(start_pose)
        # find pose of center of the semicircle to draw
        circle_center = copy.deepcopy(start_pose)
        circle_center.position.z += 0.1

        self.add_semicircle(circle_center, 0.1, 100, 1, waypoints)


        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        move_group.execute(plan, wait=True)

def main():
    try:
        print("NOTE: this must be run from the robot HOME POSITION or something reasonably nearby! If HOME position is not reasonably accessible, will likely fail to plan correctly.")
        print("starting")
        tutorial = MoveGroupPythonInterfaceTutorial()

        print("moving to start pose")
        tutorial.home()

        print("drawing D")
        tutorial.draw_d()
        tutorial.next_letter(1)
        print("drawing P")
        tutorial.draw_p()
        tutorial.next_letter(2)
        print("drawing C")
        tutorial.draw_c()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()