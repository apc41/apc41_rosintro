# !/usr/bin/bash
rosservice call /spawn 6 1 0 "turtle2"
# Now we have /turtle1 and /turtle2.

# teleport them so they start at the right place
rosservice call /turtle1/teleport_absolute 1 1 0

# Clear the screen from teleport marks
rosservice call /clear

# Set turtle pen colors
rosservice call /turtle1/set_pen 255 0 0 5 0
rosservice call /turtle2/set_pen 0 255 0 5 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[6.0, 0.0, 0.0]' '[0.0, 0.0, -3.2]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[-6.0, 0.0, 0.0]' '[0.0, 0.0, -3.2]'
